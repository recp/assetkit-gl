/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_lambert_h
#define ak_lambert_h

#include "../../../../include/ak-opengl.h"

GkLambert*
ak_glLambert(AkLambert * __restrict lambert,
             const char *routine);

#endif /* ak_lambert_h */
