/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_glblinn_h
#define ak_glblinn_h

#include "../../../../include/ak-opengl.h"

GkBlinn*
ak_glBlinn(AkBlinn * __restrict blinn,
           const char *routine);

#endif /* ak_glblinn_h */
