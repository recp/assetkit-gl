/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_constant_h
#define ak_constant_h

#include "../../../../include/ak-opengl.h"

GkConstant*
ak_glConstant(AkConstantFx * __restrict constant,
              const char *routine);

#endif /* ak_constant_h */
