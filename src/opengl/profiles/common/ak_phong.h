/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_glphong_h
#define ak_glphong_h

#include "../../../../include/ak-opengl.h"

GkPhong*
ak_glPhong(AkPhong * __restrict phong,
           const char *routine);

#endif /* ak_glphong_h */
