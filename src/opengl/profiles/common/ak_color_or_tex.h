/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_color_or_tex_h
#define ak_color_or_tex_h

#include "../../../../include/ak-opengl.h"

void
ak_glCopyColorOrTex(AkFxColorOrTex * __restrict src,
                    GkColorOrTex   * __restrict dest);

#endif /* ak_color_or_tex_h */
